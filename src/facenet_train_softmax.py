"""
Training a face recognizer using softmax, customized data input procedure
"""

import os
import time
import tensorflow as tf
import sys
import argparse
import importlib
from datetime import datetime
import facenet
import numpy as np
import random
import tensorflow.contrib.slim as slim
from scipy import misc


def main(args):
    network = importlib.import_module(args.model_def)

    subdir = datetime.strftime(datetime.now(), '%Y%m%d-%H%M%S')
    log_dir = os.path.join(os.path.expanduser(args.logs_base_dir), subdir)
    if not os.path.isdir(log_dir):
        os.makedirs(log_dir)
    model_dir = os.path.join(os.path.expanduser(args.models_base_dir), subdir)
    if not os.path.isdir(model_dir):
        os.makedirs(model_dir)
    # Write arguments to a text file
    facenet.write_arguments_to_file(args, os.path.join(log_dir, 'arguments.txt'))

    # Store some git revision info in a text file in the log directory
    src_path, _ = os.path.split(os.path.realpath(__file__))
    facenet.store_revision_info(src_path, log_dir, ' '.join(sys.argv))

    np.random.seed(seed=args.seed)
    random.seed(args.seed)

    all_data_set = facenet.get_dataset(args.data_dir)
    train_set = []
    test_set = []
    for item in all_data_set:
        paths = item.image_paths
        class_name = item.name

        index = np.arange(len(paths))

        train_path = [paths[i] for i in index[:int(np.ceil(len(paths) * 0.7))]]
        test_path = [paths[i] for i in index[int(np.ceil(len(paths) * 0.7)):]]

        train_set.append(facenet.ImageClass(class_name, train_path))
        test_set.append(facenet.ImageClass(class_name, test_path))

    test_image_list, test_label_list = facenet.get_image_paths_and_labels(test_set)

    nrof_classes = len(train_set)
    print('Model directory: %s' % model_dir)
    print('Log directory: %s' % log_dir)
    pretrained_model = None
    if args.pretrained_model:
        pretrained_model = os.path.expanduser(args.pretrained_model)
        print('Pre-trained model: %s' % pretrained_model)

    with tf.Graph().as_default():
        tf.set_random_seed(args.seed)
        global_step = tf.Variable(0, trainable=False)

        learning_rate_placeholder = tf.placeholder(tf.float32, name='learning_rate')

        phase_train_placeholder = tf.placeholder(tf.bool, name='phase_train')

        image_placeholder = tf.placeholder(tf.float32, shape=(None, args.image_size, args.image_size, 3),
                                           name='image_data')

        label_placeholder = tf.placeholder(tf.int64, shape=(None,), name='label_data')

        image_split = tf.split(image_placeholder, args.num_gpus)
        label_split = tf.split(label_placeholder, args.num_gpus)

        print('Total number of classes: %d' % nrof_classes)
        print('Building training graph')

        tower_grads = []
        reuse = False
        with tf.variable_scope(tf.get_variable_scope()):
            for i in xrange(args.num_gpus):
                with tf.device('/gpu:%d' % i):
                    with tf.name_scope('gpu_%d' % i) as scope:
                        prelogits, _ = network.inference(image_split[i], args.keep_probability,
                                                         phase_train=phase_train_placeholder,
                                                         bottleneck_layer_size=args.embedding_size,
                                                         weight_decay=args.weight_decay, reuse=reuse)
                        logits = slim.fully_connected(prelogits, len(train_set), activation_fn=None,
                                                      weights_initializer=tf.truncated_normal_initializer(stddev=0.1),
                                                      weights_regularizer=slim.l2_regularizer(args.weight_decay),
                                                      scope='Logits', reuse=reuse)

                        if i == 0:
                            test_prelogits, _ = network.inference(image_placeholder, args.keep_probability,
                                                                  phase_train=phase_train_placeholder,
                                                                  bottleneck_layer_size=args.embedding_size,
                                                                  weight_decay=args.weight_decay, reuse=True)
                            test_logits = slim.fully_connected(test_prelogits, len(train_set), activation_fn=None,
                                                               weights_initializer=tf.truncated_normal_initializer(
                                                                   stddev=0.1),
                                                               weights_regularizer=slim.l2_regularizer(
                                                                   args.weight_decay),
                                                               scope='Logits', reuse=True)

                        # Add center loss
                        if args.center_loss_factor > 0.0:
                            prelogits_center_loss, _ = facenet.center_loss(prelogits, label_split[i],
                                                                           args.center_loss_alfa, nrof_classes)
                            tf.add_to_collection(tf.GraphKeys.REGULARIZATION_LOSSES,
                                                 prelogits_center_loss * args.center_loss_factor)

                        learning_rate = tf.train.exponential_decay(learning_rate_placeholder, global_step,
                                                                   args.learning_rate_decay_epochs * args.epoch_size,
                                                                   args.learning_rate_decay_factor, staircase=True)
                        tf.summary.scalar('learning_rate', learning_rate)

                        # Calculate the average cross entropy loss across the batch
                        cross_entropy = tf.nn.sparse_softmax_cross_entropy_with_logits(
                            labels=label_split[i], logits=logits, name='cross_entropy_per_example')
                        cross_entropy_mean = tf.reduce_mean(cross_entropy, name='cross_entropy')
                        tf.add_to_collection('losses', cross_entropy_mean)

                        # Calculate the total losses
                        regularization_losses = tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES)
                        total_loss = tf.add_n([cross_entropy_mean] + regularization_losses, name='total_loss')

                        loss_averages = tf.train.ExponentialMovingAverage(0.9, name='avg')
                        losses = tf.get_collection('losses', scope)
                        loss_averages_op = loss_averages.apply(losses + [total_loss])

                        optimizer = args.optimizer
                        update_gradient_vars = tf.global_variables()
                        with tf.control_dependencies([loss_averages_op]):
                            if optimizer == 'ADAGRAD':
                                opt = tf.train.AdagradOptimizer(learning_rate)
                            elif optimizer == 'SGD':
                                opt = tf.train.GradientDescentOptimizer(learning_rate)
                            elif optimizer == 'ADADELTA':
                                opt = tf.train.AdadeltaOptimizer(learning_rate, rho=0.9, epsilon=1e-6)
                            elif optimizer == 'ADAM':
                                opt = tf.train.AdamOptimizer(learning_rate, beta1=0.9, beta2=0.999, epsilon=0.1)
                            elif optimizer == 'RMSPROP':
                                opt = tf.train.RMSPropOptimizer(learning_rate, decay=0.9, momentum=0.9, epsilon=1.0)
                            elif optimizer == 'MOM':
                                opt = tf.train.MomentumOptimizer(learning_rate, 0.9, use_nesterov=True)
                            else:
                                raise ValueError('Invalid optimization algorithm')

                            grads = opt.compute_gradients(total_loss, update_gradient_vars)

                            tower_grads.append(grads)

                            reuse = True

                            # tf.get_variable_scope().reuse_variables()

        grads = facenet.average_gradients(tower_grads)

        apply_gradient_op = opt.apply_gradients(grads, global_step=global_step)

        # Attach a scalar summmary to all individual losses and the total loss; do the
        # same for the averaged version of the losses.
        for l in losses + [total_loss]:
            # Name each loss as '(raw)' and name the moving average version of the loss
            # as the original loss name.
            tf.summary.scalar(l.op.name.split('/')[-1] + ' (raw)', l)
            tf.summary.scalar(l.op.name.split('/')[-1] + '_1', loss_averages.average(l))

        # Add histograms for trainable variables.
        if args.log_histograms:
            for var in tf.trainable_variables():
                tf.summary.histogram(var.op.name, var)

        # Add histograms for gradients.
        if args.log_histograms:
            for grad, var in grads:
                if grad is not None:
                    tf.summary.histogram(var.op.name + '/gradients', grad)

        # Track the moving averages of all trainable variables.
        variable_averages = tf.train.ExponentialMovingAverage(
            args.moving_average_decay, global_step)
        variables_averages_op = variable_averages.apply(tf.trainable_variables())
        with tf.control_dependencies([apply_gradient_op, variables_averages_op]):
            train_op = tf.no_op(name='train')

        # train_op = facenet.train(total_loss, global_step, args.optimizer,
        #                          learning_rate, args.moving_average_decay, tf.global_variables(), args.log_histograms)

        # Create a saver
        use_facenet_model = False
        if use_facenet_model:
            model_loader = tf.train.Saver(tf.trainable_variables()[:-2], max_to_keep=3)
        else:
            model_loader = tf.train.Saver(tf.trainable_variables(), max_to_keep=3)
        saver = tf.train.Saver(tf.trainable_variables(), max_to_keep=3)

        # Build the summary operation based on the TF collection of Summaries.
        summary_op = tf.summary.merge_all()

        # Start running operations on the Graph.
        gpu_options = tf.GPUOptions(allow_growth=True)
        sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options, log_device_placement=False,
                                                allow_soft_placement=True))
        sess.run(tf.global_variables_initializer())
        sess.run(tf.local_variables_initializer())
        summary_writer = tf.summary.FileWriter(log_dir, sess.graph)

        with sess.as_default():
            if pretrained_model:
                print('Restoring pretrained model: %s' % pretrained_model)
                model_loader.restore(sess, pretrained_model)
                # facenet.load_model(pretrained_model)
            print('Running training')
            epoch = 0
            while epoch < args.max_nrof_epochs:
                step = sess.run(global_step, feed_dict=None)
                train(args, sess, epoch, train_set, image_placeholder, label_placeholder, learning_rate_placeholder,
                      phase_train_placeholder, global_step, total_loss, train_op,
                      summary_op, summary_writer, regularization_losses)
                evaluate(args, sess, test_image_list, test_label_list, image_placeholder, phase_train_placeholder,
                         test_logits, global_step, summary_op, summary_writer)
                epoch += 1
                # Save variables and the metagraph if it doesn't exist already
                save_variables_and_metagraph(sess, saver, summary_writer, model_dir, subdir, step)

    return model_dir


def train(args, sess, epoch, train_set, image_placeholder, label_placeholder, learning_rate_placeholder,
          phase_train_placeholder, global_step, loss, train_op, summary_op, summary_writer, regularization_losses):
    batch_number = 0

    lr = args.learning_rate

    step = sess.run(global_step)

    # batch_idx = np.random.permutation(np.arange(len(train_set)))
    batch_idx = np.random.randint(0, len(train_set), size=(args.epoch_size, args.batch_size / 2))

    # Training loop
    train_time = 0
    while batch_number < args.epoch_size:
        i_idx = batch_idx[batch_number]
        start_time = time.time()

        input_batch = [train_set[i] for i in i_idx]

        image_batch = np.zeros((args.batch_size, args.image_size, args.image_size, 3))
        label_batch = np.zeros((args.batch_size))

        i_batch = 0
        for index in range(len(input_batch)):
            img_idx = np.random.permutation(np.arange(len(input_batch[index].image_paths)))
            for i_img in range(2):
                img = misc.imread(input_batch[index].image_paths[img_idx[i_img]])
                # TODO: image padding and resize
                image_batch[i_batch, :, :, :] = facenet.prewhiten(img)
                label_batch[i_batch] = i_idx[index]
                i_batch += 1

        feed_dict = {learning_rate_placeholder: lr, phase_train_placeholder: True,
                     image_placeholder: image_batch, label_placeholder: label_batch}
        if batch_number % 100 == 0:
            err, _, step, reg_loss, summary_str = sess.run(
                [loss, train_op, global_step, regularization_losses, summary_op], feed_dict=feed_dict)
            summary_writer.add_summary(summary_str, global_step=step)
        else:
            err, _, step, reg_loss = sess.run([loss, train_op, global_step, regularization_losses], feed_dict=feed_dict)
        duration = time.time() - start_time
        print('Epoch: [%d][%d/%d]\tTime %.3f\tLoss %2.3f\tRegLoss %2.3f' %
              (epoch, batch_number + 1, args.epoch_size, duration, err, np.sum(reg_loss)))
        batch_number += 1
        train_time += duration
    # Add validation loss and accuracy to summary
    summary = tf.Summary()
    # pylint: disable=maybe-no-member
    summary.value.add(tag='time/total', simple_value=train_time)
    summary_writer.add_summary(summary, step)
    return step


def evaluate(args, sess, image_list, label_list, image_placeholder, phase_train_placeholder,
             logits, global_step, summary_op, summary_writer):
    test_size = len(image_list) // (args.batch_size / args.num_gpus)
    test_batch = [(args.batch_size / args.num_gpus)] * test_size
    if (args.batch_size / args.num_gpus) * test_size != len(image_list):
        test_batch += [len(image_list) - (args.batch_size / args.num_gpus) * test_size]
        test_size += 1

    print('Test: ')

    data_prepare_time = time.time()
    randperm = np.random.permutation(len(image_list))

    image_list = [image_list[i] for i in randperm]
    label_list = [label_list[i] for i in randperm]

    data_prepare_time = time.time() - data_prepare_time

    print('Data_prepare: %f' % data_prepare_time)

    acc = 0
    for i in range(test_size):
        data_process_time = time.time()
        image_batch = np.zeros((test_batch[i], args.image_size, args.image_size, 3))
        label_batch = np.zeros((test_batch[i]))

        for i_img in range(test_batch[i]):
            img = misc.imread(image_list[i * (args.batch_size / args.num_gpus) + i_img])
            image_batch[i_img] = facenet.prewhiten(img)
            label_batch[i_img] = label_list[i * (args.batch_size / args.num_gpus) + i_img]

        feed_dict = {phase_train_placeholder: False,
                     image_placeholder: image_batch}  # , label_placeholder: label_batch}

        data_process_time = time.time() - data_process_time

        inference_time = time.time()
        predict = sess.run(logits, feed_dict=feed_dict)

        predict = np.argmax(predict, axis=1)

        inference_time = time.time() - inference_time

        acc += sum(
            np.equal(predict, label_batch).astype('int')) / float(  # [:args.batch_size // args.num_gpus]
            len(predict))

        print('\t(%d/%d) Data_process: %f\tInference: %f' % (i + 1, test_size, data_process_time, inference_time))

    print('Accuracy: %f' % (acc / float(test_size)))

    step = sess.run(global_step)

    summary = tf.Summary()
    summary.value.add(tag='Accuracy', simple_value=(acc / float(test_size)))
    summary_writer.add_summary(summary, step)

    return acc / float(test_size)


def save_variables_and_metagraph(sess, saver, summary_writer, model_dir, model_name, step):
    # Save the model checkpoint
    print('Saving variables')
    start_time = time.time()
    checkpoint_path = os.path.join(model_dir, 'model-%s.ckpt' % model_name)
    saver.save(sess, checkpoint_path, global_step=step, write_meta_graph=False)
    save_time_variables = time.time() - start_time
    print('Variables saved in %.2f seconds' % save_time_variables)
    metagraph_filename = os.path.join(model_dir, 'model-%s.meta' % model_name)
    save_time_metagraph = 0
    if not os.path.exists(metagraph_filename):
        print('Saving metagraph')
        start_time = time.time()
        saver.export_meta_graph(metagraph_filename)
        save_time_metagraph = time.time() - start_time
        print('Metagraph saved in %.2f seconds' % save_time_metagraph)
    summary = tf.Summary()
    # pylint: disable=maybe-no-member
    summary.value.add(tag='time/save_variables', simple_value=save_time_variables)
    summary.value.add(tag='time/save_metagraph', simple_value=save_time_metagraph)
    summary_writer.add_summary(summary, step)


def parse_arguments(argv):
    parser = argparse.ArgumentParser()

    parser.add_argument('--logs_base_dir', type=str,
                        help='Directory where to write event logs.', default='../logs/')
    parser.add_argument('--models_base_dir', type=str,
                        help='Directory where to write trained models and checkpoints.', default='../models/')
    parser.add_argument('--pretrained_model', type=str,
                        help='Load a pretrained model before training starts.')
    parser.add_argument('--data_dir', type=str,
                        help='Path to the data directory containing aligned face patches. Multiple directories are separated with colon.')
    parser.add_argument('--model_def', type=str,
                        help='Model definition. Points to a module containing the definition of the inference graph.',
                        default='models.inception_resnet_v1')
    parser.add_argument('--max_nrof_epochs', type=int,
                        help='Number of epochs to run.', default=50000)
    parser.add_argument('--batch_size', type=int,
                        help='Number of images to process in a batch.', default=90)
    parser.add_argument('--image_size', type=int,
                        help='Image size (height, width) in pixels.', default=160)
    parser.add_argument('--epoch_size', type=int,
                        help='Number of batches per epoch.', default=1000)
    parser.add_argument('--embedding_size', type=int,
                        help='Dimensionality of the embedding.', default=128)
    parser.add_argument('--keep_probability', type=float,
                        help='Keep probability of dropout for the fully connected layer(s).', default=1.0)
    parser.add_argument('--weight_decay', type=float,
                        help='L2 weight regularization.', default=0.0)
    parser.add_argument('--optimizer', type=str, choices=['SGD', 'ADAGRAD', 'ADADELTA', 'ADAM', 'RMSPROP', 'MOM'],
                        help='The optimization algorithm to use', default='SGD')
    parser.add_argument('--learning_rate', type=float,
                        help='Initial learning rate. If set to a negative value a learning rate ' +
                             'schedule can be specified in the file "learning_rate_schedule.txt"', default=0.1)
    parser.add_argument('--learning_rate_decay_epochs', type=int,
                        help='Number of epochs between learning rate decay.', default=100)
    parser.add_argument('--learning_rate_decay_factor', type=float,
                        help='Learning rate decay factor.', default=1.0)
    parser.add_argument('--log_histograms',
                        help='Enables logging of weight/bias histograms in tensorboard.', action='store_true')
    parser.add_argument('--seed', type=int,
                        help='Random seed.', default=777)
    parser.add_argument('--center_loss_factor', type=float,
                        help='Center loss factor.', default=0.0)
    parser.add_argument('--center_loss_alfa', type=float,
                        help='Center update rate for center loss.', default=0.95)
    parser.add_argument('--moving_average_decay', type=float,
                        help='Exponential decay for tracking of training parameters.', default=0.9999)
    parser.add_argument('--num_gpus', type=int,
                        help='Number of gpu to be used.')
    parser.add_argument('--test_size', type=int,
                        help='Number of test samples per test.', default=100)

    return parser.parse_args(argv)


if __name__ == '__main__':
    main(parse_arguments(sys.argv[1:]))

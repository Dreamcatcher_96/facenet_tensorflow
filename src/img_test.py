import facenet
from scipy import misc

path = '/media/alan/Data/Face_data/face_data_washed_aligned/291589100/291589100_00_306.jpg'

img = misc.imresize(misc.imread(path), (160, 160))
img = facenet.prewhiten(img)
print img

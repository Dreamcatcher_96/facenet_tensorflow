import multiprocessing
import tensorflow as tf
import numpy as np
import facenet
import os
import time
from scipy import misc
import models.inception_resnet_v1 as network
import tensorflow.contrib.slim as slim
import pylab as pl

data_dir = '/media/alan/Data/Face_data/face_data_washed_aligned'
model_path = ['/home/alan/workspace/Facenet_tensorflow/models/20171202-111254/model-20171202-111254.ckpt-11000',
              '/home/alan/workspace/Facenet_tensorflow/models/20171202-111254/model-20171202-111254.ckpt-12000',
              '/home/alan/workspace/Facenet_tensorflow/models/20171202-111254/model-20171202-111254.ckpt-13000']


# ckpts = [246, 247]


def load_dataset(dataset):
    loaded_imgs = []
    multiprocess = False
    if multiprocess:
        nof_people = len(dataset)

        image_list, label_list = facenet.get_image_paths_and_labels(dataset)
        '/home/alan/workspace/Facenet_tensorflow/models/20171202-111254/model-20171202-111254.ckpt-12000.index'
        data_set = zip(image_list, range(len(image_list)),
                       [len(image_list)] * (len(image_list)))

        proc = facenet.Processor()
        pool = multiprocessing.Pool()
        train_set = np.asarray(pool.map(proc, data_set))

        for idx in range(nof_people):
            loaded_imgs.append(train_set[np.asarray(label_list) == idx])
    else:
        for i_img in range(len(dataset)):
            image_set = np.zeros((len(dataset[i_img].image_paths), 150, 150, 3),
                                 dtype=np.uint8)
            for i_idx in range(len(dataset[i_img].image_paths)):
                image_set[i_idx, :, :, :] = misc.imread(dataset[i_img].image_paths[i_idx])
            loaded_imgs.append(image_set)
            print('Loading images: %.1f%%' % (float(i_img + 1) / len(dataset) * 100))

    return loaded_imgs


def evaluate(sess, embeddings, image_batch, label_batch, image_placeholder,
             phase_train_placeholder):
    batch_size = 60
    embedding_size = 128
    start_time = time.time()
    print('Test:')

    print('\tData prepare: %f' % (time.time() - start_time))

    nrof_batch = np.ceil(len(image_batch) / float(batch_size))
    emb_array = np.zeros((len(image_batch), embedding_size))

    batch_size = int(batch_size)
    for i in range(int(nrof_batch)):
        start_time = time.time()
        right_bound = min((i + 1) * batch_size, len(image_batch))

        feed_dict = {image_placeholder: image_batch[i * batch_size:right_bound], phase_train_placeholder: False}

        emb = sess.run(embeddings, feed_dict=feed_dict)
        emb_array[i * batch_size:right_bound, :] = emb
        print('\t(%d/%d): %f' % (i + 1, nrof_batch, (time.time() - start_time)))

    # _, _, accuracy, val, val_std, far = lfw.evaluate(emb_array, label_batch)

    thresholds = np.arange(0, 4, 0.01)
    embeddings1 = emb_array[0::2]
    embeddings2 = emb_array[1::2]
    assert (embeddings1.shape[0] == embeddings2.shape[0])
    assert (embeddings1.shape[1] == embeddings2.shape[1])

    diff = np.subtract(embeddings1, embeddings2)
    dist = np.sum(np.square(diff), 1)

    accuracy = []
    tps = []
    tns = []
    for idx in range(len(thresholds)):
        predict_issame = np.less(dist[:len(label_batch)], thresholds[idx])
        tp = float(np.sum(np.logical_and(predict_issame, label_batch)))
        fp = float(np.sum(np.logical_and(predict_issame, np.logical_not(label_batch))))
        tn = float(np.sum(np.logical_and(np.logical_not(predict_issame), np.logical_not(label_batch))))
        fn = float(np.sum(np.logical_and(np.logical_not(predict_issame), label_batch)))

        acc = (tp / np.sum(label_batch) + tn / np.sum(np.logical_not(label_batch))) / 2
        tps.append(tp / np.sum(label_batch))
        tns.append(tn / np.sum(np.logical_not(label_batch)))
        accuracy.append(acc)
    best_threshold_index = np.argmax(accuracy)
    print('Accuracy: %1.3f  tp: %1.3f  tn:%1.3f  threshold: %1.3f' % (accuracy[best_threshold_index],
                                                                      tps[best_threshold_index],
                                                                      tns[best_threshold_index],
                                                                      thresholds[best_threshold_index]))

    return accuracy[best_threshold_index]


def main():
    accs = []
    all_data_set = facenet.get_dataset(data_dir)
    rand_idx = np.load('../data/train_test_idx.npy')
    dataset = [all_data_set[i] for i in rand_idx[:5000]]

    test_size = len(dataset) / 2
    image_size = 150
    image_batch = np.zeros((test_size * 20, image_size, image_size, 3))
    label_batch = []
    rand_idx = np.random.permutation(len(dataset))
    start_idx = 0
    for i in range(test_size):
        img_path = dataset[rand_idx[i]].image_paths
        rand_pos_idx = np.random.permutation(len(img_path))

        for it in range(min(5, int(len(rand_pos_idx) / 2))):
            image_batch[start_idx, :, :, :] = facenet.prewhiten(misc.imread(img_path[rand_pos_idx[it]]))
            image_batch[start_idx + 1, :, :, :] = facenet.prewhiten(misc.imread(
                img_path[rand_pos_idx[it + min(5, int(len(rand_pos_idx) / 2))]]))
            label_batch.append(True)
            start_idx += 2
        for it in range(min(5, len(img_path))):
            neg_idx = np.random.randint(0, len(dataset), size=1)[0]
            while neg_idx == rand_idx[i]:
                neg_idx = np.random.randint(0, len(dataset), size=1)[0]
            image_batch[start_idx, :, :, :] = facenet.prewhiten(misc.imread(img_path[it]))
            image_batch[start_idx + 1, :, :, :] = \
                facenet.prewhiten(
                    misc.imread(dataset[neg_idx].image_paths[np.random.randint(0, len(dataset[neg_idx]))]))
            label_batch.append(False)
            start_idx += 2
        print '%d/%d %.1f%%' % (i, test_size, float(i) / test_size * 100)

    for path in model_path:
        with tf.Graph().as_default():
            with tf.Session() as sess:
                # test_imgs = load_dataset(test_set)

                # facenet.load_model(model_path)

                images_placeholder = tf.placeholder(tf.float32, shape=(None, 150, 150, 3),
                                                    name='image_data')
                # logits = tf.get_default_graph().get_tensor_by_name('Logits/BiasAdd:0')
                phase_train_placeholder = tf.placeholder(tf.bool, name='phase_train')

                prelogits, _ = network.inference(images_placeholder, 1,
                                                 phase_train=phase_train_placeholder)
                embeddings = tf.nn.l2_normalize(prelogits, 1, 1e-10, name='embeddings')

                # sess.run(tf.initialize_all_variables())

                saver = tf.train.Saver(tf.trainable_variables())

                saver.restore(sess, path)

                # variables = tf.trainable_variables()
                #
                # server_weight = np.load('weight_when_save.npy')
                #
                # # for item in variables:
                # weight = sess.run(variables[0]._variable)
                # np.save('weight', weight)

                acc = evaluate(sess, embeddings, image_batch, label_batch, images_placeholder, phase_train_placeholder)

                accs.append(acc)

    axis = range(1, len(accs) + 1)
    pl.plot(axis, accs)
    pl.show()
    print accs


if __name__ == '__main__':
    main()

import time
import six
import numpy as np
import tensorflow as tf
from tensorflow.contrib import slim
from tensorflow.core.framework import node_def_pb2
from tensorflow.python.framework import device as pydev
from tensorflow.python.training import device_setter
import models.inception_resnet_v1 as network
import facenet
from tensorflow.python.client import timeline

seed = 777
image_size = 150
num_gpus_list = [2]
keep_probability = 0.9
embedding_size = 128
weight_decay = 5e-5
alpha = 0.2
epoch_size = 1000
variable_strategy = 'CPU'


def slim_conv_net(x, n_classes, dropout, reuse, is_training, scope='InceptionResnetV1'):
    end_points = {}
    # Define a scope for reusing the variables
    with tf.variable_scope(scope, 'InceptionResnetV1', [x], reuse=reuse):
        with slim.arg_scope([slim.batch_norm, slim.dropout],
                            is_training=is_training):
            with slim.arg_scope([slim.conv2d, slim.max_pool2d, slim.avg_pool2d],
                                stride=1, padding='SAME'):
                # 149 x 149 x 32
                net = slim.conv2d(x, 32, 3, stride=2, padding='VALID',
                                  scope='Conv2d_1a_3x3')
                end_points['Conv2d_1a_3x3'] = net
                # 147 x 147 x 32
                net = slim.conv2d(net, 32, 3, padding='VALID',
                                  scope='Conv2d_2a_3x3')
                end_points['Conv2d_2a_3x3'] = net
                # 147 x 147 x 64
                net = slim.conv2d(net, 64, 3, scope='Conv2d_2b_3x3')
                end_points['Conv2d_2b_3x3'] = net
                # 73 x 73 x 64
                net = slim.max_pool2d(net, 3, stride=2, padding='VALID',
                                      scope='MaxPool_3a_3x3')
                end_points['MaxPool_3a_3x3'] = net
                # 73 x 73 x 80
                net = slim.conv2d(net, 80, 1, padding='VALID',
                                  scope='Conv2d_3b_1x1')
                end_points['Conv2d_3b_1x1'] = net
                # 71 x 71 x 192
                net = slim.conv2d(net, 192, 3, padding='VALID',
                                  scope='Conv2d_4a_3x3')
                end_points['Conv2d_4a_3x3'] = net
                # 35 x 35 x 256
                net = slim.conv2d(net, 256, 3, stride=2, padding='VALID',
                                  scope='Conv2d_4b_3x3')
                end_points['Conv2d_4b_3x3'] = net

                with tf.variable_scope('Logits'):
                    end_points['PrePool'] = net
                    # pylint: disable=no-member
                    net = slim.avg_pool2d(net, net.get_shape()[1:3], padding='VALID',
                                          scope='AvgPool_1a_8x8')
                    net = slim.flatten(net)

                    net = slim.dropout(net, dropout, is_training=is_training,
                                       scope='Dropout')

                    end_points['PreLogitsFlatten'] = net

                net = slim.fully_connected(net, n_classes, activation_fn=None,
                                           scope='Bottleneck', reuse=False)

    return net


def conv_net(x, n_classes, dropout, reuse, is_training):
    # Define a scope for reusing the variables
    with tf.variable_scope('ConvNet', reuse=reuse):
        # MNIST data input is a 1-D vector of 784 features (28*28 pixels)
        # Reshape to match picture format [Height x Width x Channel]
        # Tensor input become 4-D: [Batch Size, Height, Width, Channel]
        # x = tf.reshape(x, shape=[-1, 28, 28, 1])

        # Convolution Layer with 64 filters and a kernel size of 5
        x = tf.layers.conv2d(x, 64, 5, activation=tf.nn.relu)
        # Max Pooling (down-sampling) with strides of 2 and kernel size of 2
        x = tf.layers.max_pooling2d(x, 2, 2)

        # Convolution Layer with 256 filters and a kernel size of 5
        x = tf.layers.conv2d(x, 64, 3, activation=tf.nn.relu)
        # Convolution Layer with 512 filters and a kernel size of 5
        x = tf.layers.conv2d(x, 64, 3, activation=tf.nn.relu)
        # Max Pooling (down-sampling) with strides of 2 and kernel size of 2
        x = tf.layers.max_pooling2d(x, 2, 2)

        # Flatten the data to a 1-D vector for the fully connected layer
        x = tf.contrib.layers.flatten(x)

        # Fully connected layer (in contrib folder for now)
        x = tf.layers.dense(x, 512)
        # Apply Dropout (if is_training is False, dropout is not applied)
        x = tf.layers.dropout(x, rate=dropout, training=is_training)

        # Fully connected layer (in contrib folder for now)
        x = tf.layers.dense(x, 128)
        # Apply Dropout (if is_training is False, dropout is not applied)
        x = tf.layers.dropout(x, rate=dropout, training=is_training)

        # Output layer, class prediction
        out = tf.layers.dense(x, n_classes)
        # Because 'softmax_cross_entropy_with_logits' loss already apply
        # softmax, we only apply softmax to testing network
        # out = tf.nn.softmax(out) if not is_training else out

    return out


def local_device_setter(num_devices=1,
                        ps_device_type='cpu',
                        worker_device='/cpu:0',
                        ps_ops=None,
                        ps_strategy=None):
    if ps_ops is None:
        ps_ops = ['Variable', 'VariableV2', 'VarHandleOp']

    if ps_strategy is None:
        ps_strategy = device_setter._RoundRobinStrategy(num_devices)
    if not six.callable(ps_strategy):
        raise TypeError("ps_strategy must be callable")

    def _local_device_chooser(op):
        current_device = pydev.DeviceSpec.from_string(op.device or "")

        node_def = op if isinstance(op, node_def_pb2.NodeDef) else op.node_def
        if node_def.op in ps_ops:
            ps_device_spec = pydev.DeviceSpec.from_string(
                '/{}:{}'.format(ps_device_type, ps_strategy(op)))

            ps_device_spec.merge_from(current_device)
            return ps_device_spec.to_string()
        else:
            worker_device_spec = pydev.DeviceSpec.from_string(worker_device or "")
            worker_device_spec.merge_from(current_device)
            return worker_device_spec.to_string()

    return _local_device_chooser


times = []
samples = []
for num_gpus in num_gpus_list:
    with tf.Graph().as_default(), tf.device('/cpu:0'):
        tf.set_random_seed(seed)
        global_step = tf.Variable(0, trainable=False)

        # Placeholder for the learning rate
        learning_rate_placeholder = tf.placeholder(tf.float32, name='learning_rate')

        phase_train_placeholder = tf.placeholder(tf.bool, name='phase_train')

        image_placeholder = tf.placeholder(tf.float32, shape=(None, image_size, image_size, 3),
                                           name='image_data')

        # label_placeholder = tf.placeholder(tf.int64, shape=(None,), name='label_data')

        image_split = tf.split(image_placeholder, num_gpus)
        # label_split = tf.split(label_placeholder, num_gpus)

        learning_rate = tf.train.exponential_decay(learning_rate_placeholder, global_step,
                                                   100 * epoch_size,
                                                   1.0, staircase=True)
        tf.summary.scalar('learning_rate', learning_rate)

        tower_grads = []
        reuse = False
        embeddings_set = []
        if num_gpus == 0:
            num_devices = 1
            device_type = 'cpu'
        else:
            num_devices = num_gpus
            device_type = 'gpu'
        for i in xrange(num_gpus):
            worker_device = '/{}:{}'.format(device_type, i)
            if variable_strategy == 'CPU':
                model_device_setter = local_device_setter(
                    worker_device=worker_device)
            elif variable_strategy == 'GPU':
                model_device_setter = local_device_setter(
                    ps_device_type='gpu',
                    worker_device=worker_device,
                    ps_strategy=tf.contrib.training.GreedyLoadBalancingStrategy(
                        num_gpus, tf.contrib.training.byte_size_load_fn))
            with tf.variable_scope(tf.get_variable_scope()):
                with tf.name_scope('tower_%d' % i) as scope:
                    with tf.device(model_device_setter):
                        # Build the inference graph
                        prelogits, _ = network.inference(image_split[i], keep_probability,
                                                         phase_train=phase_train_placeholder,
                                                         bottleneck_layer_size=embedding_size,
                                                         weight_decay=weight_decay, reuse=reuse)

                        # prelogits = conv_net(image_split[i], embedding_size, keep_probability,
                        #                      reuse=reuse, is_training=phase_train_placeholder)

                        # prelogits, _ = network.inception_resnet_v1(image_split[i], is_training=phase_train_placeholder,
                        #                                            reuse=reuse)

                        embeddings = tf.nn.l2_normalize(prelogits, 1, 1e-10, name='embeddings')

                        embeddings_set.append(embeddings)

                        # Split embeddings into anchor, positive and negative and calculate triplet loss
                        anchor, positive, negative = tf.unstack(tf.reshape(embeddings, [-1, 3, embedding_size]), 3,
                                                                1)
                        triplet_loss = facenet.triplet_loss(anchor, positive, negative, alpha)

                        # Calculate the total losses
                        regularization_losses = tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES)
                        total_loss = tf.add_n([triplet_loss] + regularization_losses, name='total_loss')

                        # Build a Graph that trains the model with one batch of examples and updates the model
                        # parameters
                        # loss_averages = tf.train.ExponentialMovingAverage(0.9, name='avg')
                        # losses = tf.get_collection('losses', scope)
                        # loss_averages_op = loss_averages.apply(losses + [total_loss])

                        optimizer = 'SGD'
                        update_gradient_vars = tf.global_variables()
                        opt = tf.train.GradientDescentOptimizer(learning_rate)
                        grads = opt.compute_gradients(total_loss, update_gradient_vars)

                        tower_grads.append(grads)

                        reuse = True
                        if i == 0:
                            update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS,
                                                           scope)

        embed_group = tf.concat(embeddings_set, 0)
        grads = facenet.average_gradients(tower_grads)

        apply_gradient_op = [opt.apply_gradients(grads, global_step=global_step)]
        apply_gradient_op.extend(update_ops)

        variable_averages = tf.train.ExponentialMovingAverage(
            0.9999, global_step)
        variables_averages_op = variable_averages.apply(tf.trainable_variables())
        apply_gradient_op.append(variables_averages_op)
        train_op = tf.group(*apply_gradient_op)

        gpu_options = tf.GPUOptions(allow_growth=True)
        sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options, log_device_placement=False,
                                                allow_soft_placement=True))
        # Initialize variables
        sess.run(tf.global_variables_initializer(), feed_dict={phase_train_placeholder: True})
        sess.run(tf.local_variables_initializer(), feed_dict={phase_train_placeholder: True})

        total_time = 0
        total_sample = 0
        for i in range(5):
            input_array = np.random.rand(30 * 3 * num_gpus, 150, 150, 3)
            sess.run(embeddings_set,
                     feed_dict={image_placeholder: input_array, phase_train_placeholder: False,
                                learning_rate_placeholder: 0.1})
        for i in range(100):
            input_array = np.random.rand(30 * 3 * num_gpus, 150, 150, 3)
            start_time = time.time()
            run_metadata = tf.RunMetadata()
            sess.run(embeddings_set,
                     feed_dict={image_placeholder: input_array, phase_train_placeholder: False,
                                learning_rate_placeholder: 0.1},
                     options=tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE),
                     run_metadata=run_metadata)
            duration = time.time() - start_time
            total_time += duration
            total_sample += 30 * 3 * num_gpus / duration
            print '%d/%d  time: %.3f, %.3f Examples/sec' % (i + 1, 100, duration, 30 * num_gpus / duration)
            if i % 10 == 0:
                trace = timeline.Timeline(step_stats=run_metadata.step_stats)
                trace_file = open('timeline.ctf.json', 'w')
                trace_file.write(trace.generate_chrome_trace_format())
        print 'avg time: %.3f  avg Examples/sec: %.3f' % (total_time / 100, total_sample / 100)
        times.append(total_time / 100)
        samples.append(total_sample / 100)
print '%.3f %.3f' % (times[1] / times[0], samples[1] / samples[0])

import tensorflow as tf
import facenet
from scipy import misc
import numpy as np
import time
import models.inception_resnet_v1 as network

model_path = '/home/alan/workspace/Facenet_tensorflow/models/20171122-174446/model-20171122-174446.ckpt-66518'
img_path = ''

with tf.Graph().as_default():
    with tf.Session() as sess:
        # facenet.load_model(model_path)

        images_placeholder = tf.placeholder(tf.float32, shape=(None, 150, 150, 3),
                                            name='image_data')
        phase_train_placeholder = tf.placeholder(tf.bool, name='phase_train')

        prelogits, _ = network.inference(images_placeholder, 1,
                                         phase_train=phase_train_placeholder)
        embeddings = tf.nn.l2_normalize(prelogits, 1, 1e-10, name='embeddings')

        saver = tf.train.Saver(tf.trainable_variables())

        saver.restore(sess, model_path)

        image_batch = np.zeros((1, 150, 150, 3))
        img = misc.imread('/media/alan/Data/Face_data/face_data_washed_aligned/1281/1281_00_1.jpg')
        img = facenet.prewhiten(img)
        image_batch[0, :, :, :] = img

        feed_dict = {images_placeholder: image_batch, phase_train_placeholder: False}

        for i in range(10):
            start_time = time.time()
            sess.run(embeddings, feed_dict=feed_dict)
            end_time = time.time() - start_time

        for num in range(1, 2):
            image_batch = np.zeros((num, 150, 150, 3))
            for idx in range(num):
                img = misc.imread('/media/alan/Data/Face_data/face_data_washed_aligned/1281/1281_00_1.jpg')
                img = facenet.prewhiten(img)
                image_batch[idx, :, :, :] = img

            feed_dict = {images_placeholder: image_batch, phase_train_placeholder: False}

            total_time = 0
            for i in range(100):
                start_time = time.time()
                sess.run(embeddings, feed_dict=feed_dict)
                end_time = time.time() - start_time
                total_time += end_time
                # print end_time
            total_time /= 100
            print 'batch_size: %d  avg_time: %fms' % (num, (total_time * 1000))

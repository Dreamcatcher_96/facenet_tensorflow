"""
Training a face recognizer with TensorFlow based on the FaceNet paper
FaceNet: A Unified Embedding for Face Recognition and Clustering: http://arxiv.org/abs/1503.03832
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from datetime import datetime
import os.path
import time
import sys

import multiprocessing
import tensorflow as tf
import numpy as np
import importlib
import argparse
from scipy import misc
import facenet
import itertools
from numpy.random import permutation as randperm


def main(args):
    network = importlib.import_module(args.model_def)

    subdir = datetime.strftime(datetime.now(), '%Y%m%d-%H%M%S')
    log_dir = os.path.join(os.path.expanduser(args.logs_base_dir), subdir)
    if not os.path.isdir(log_dir):  # Create the log directory if it doesn't exist
        os.makedirs(log_dir)
    model_dir = os.path.join(os.path.expanduser(args.models_base_dir), subdir)
    if not os.path.isdir(model_dir):  # Create the model directory if it doesn't exist
        os.makedirs(model_dir)
    # Write arguments to a text file
    facenet.write_arguments_to_file(args, os.path.join(log_dir, 'arguments.txt'))

    # Store some git revision info in a text file in the log directory
    src_path, _ = os.path.split(os.path.realpath(__file__))
    facenet.store_revision_info(src_path, log_dir, ' '.join(sys.argv))

    np.random.seed(seed=args.seed)

    all_data_set = facenet.get_dataset(args.data_dir)
    # train_set = []
    # test_set = []
    if os.path.exists('../data/train_test_idx.npy'):
        rand_idx = np.load('../data/train_test_idx.npy')
    else:
        rand_idx = np.random.permutation(len(all_data_set))
        np.save('../data/train_test_idx.npy', rand_idx)

    train_set = [all_data_set[i] for i in rand_idx[5000:]]
    test_set = [all_data_set[i] for i in rand_idx[:args.test_people]]

    start_time = time.time()
    test_imgs = load_dataset(args, test_set)
    print('data load: %.3f' % (time.time() - start_time))

    print('Model directory: %s' % model_dir)
    print('Log directory: %s' % log_dir)
    if args.pretrained_model:
        print('Pre-trained model: %s' % os.path.expanduser(args.pretrained_model))

    with tf.Graph().as_default(), tf.device('/cpu:0'):
        tf.set_random_seed(args.seed)
        global_step = tf.Variable(0, trainable=False)

        # Placeholder for the learning rate
        learning_rate_placeholder = tf.placeholder(tf.float32, name='learning_rate')

        phase_train_placeholder = tf.placeholder(tf.bool, name='phase_train')

        image_placeholder = tf.placeholder(tf.float32, shape=(None, args.image_size, args.image_size, 3),
                                           name='image_data')

        # label_placeholder = tf.placeholder(tf.int64, shape=(None,), name='label_data')

        image_split = tf.split(image_placeholder, args.num_gpus)
        # label_split = tf.split(label_placeholder, args.num_gpus)

        # loss_averages = tf.train.ExponentialMovingAverage(0.9, name='avg')

        learning_rate = tf.train.exponential_decay(learning_rate_placeholder, global_step,
                                                   args.learning_rate_decay_epochs * args.epoch_size,
                                                   args.learning_rate_decay_factor, staircase=True)
        tf.summary.scalar('learning_rate', learning_rate)

        tower_grads = []
        reuse = False
        embeddings_set = []
        if args.num_gpus == 0:
            num_devices = 1
            device_type = 'cpu'
        else:
            num_devices = args.num_gpus
            device_type = 'gpu'
        for i in xrange(num_devices):
            worker_device = '/{}:{}'.format(device_type, i)
            if args.variable_strategy == 'CPU':
                model_device_setter = facenet.local_device_setter(
                    worker_device=worker_device)
            elif args.variable_strategy == 'GPU':
                model_device_setter = facenet.local_device_setter(
                    ps_device_type='gpu',
                    worker_device=worker_device,
                    ps_strategy=tf.contrib.training.GreedyLoadBalancingStrategy(
                        args.num_gpus, tf.contrib.training.byte_size_load_fn))
            with tf.variable_scope(tf.get_variable_scope()):
                with tf.name_scope('tower_%d' % i) as scope:
                    with tf.device(model_device_setter):
                        # Build the inference graph
                        prelogits, _ = network.inference(image_split[i], args.keep_probability,
                                                         phase_train=phase_train_placeholder,
                                                         bottleneck_layer_size=args.embedding_size,
                                                         weight_decay=args.weight_decay, reuse=reuse)

                        embeddings = tf.nn.l2_normalize(prelogits, 1, 1e-10, name='embeddings')
                        embeddings_set.append(embeddings)

                        # Split embeddings into anchor, positive and negative and calculate triplet loss
                        anchor, positive, negative = tf.unstack(tf.reshape(embeddings, [-1, 3, args.embedding_size]), 3,
                                                                1)
                        triplet_loss = facenet.triplet_loss(anchor, positive, negative, args.alpha)

                        # Calculate the total losses
                        regularization_losses = tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES)
                        total_loss = tf.add_n([triplet_loss] + regularization_losses, name='total_loss')

                        # Build a Graph that trains the model with one batch of examples and updates the model
                        # parameters
                        # losses = tf.get_collection('losses', scope)
                        # loss_averages_op = loss_averages.apply(losses + [total_loss])

                        optimizer = args.optimizer
                        update_gradient_vars = tf.global_variables()
                        # with tf.control_dependencies([loss_averages_op]):
                        if optimizer == 'ADAGRAD':
                            opt = tf.train.AdagradOptimizer(learning_rate)
                        elif optimizer == 'SGD':
                            opt = tf.train.GradientDescentOptimizer(learning_rate)
                        elif optimizer == 'ADADELTA':
                            opt = tf.train.AdadeltaOptimizer(learning_rate, rho=0.9, epsilon=1e-6)
                        elif optimizer == 'ADAM':
                            opt = tf.train.AdamOptimizer(learning_rate, beta1=0.9, beta2=0.999, epsilon=0.1)
                        elif optimizer == 'RMSPROP':
                            opt = tf.train.RMSPropOptimizer(learning_rate, decay=0.9, momentum=0.9, epsilon=1.0)
                        elif optimizer == 'MOM':
                            opt = tf.train.MomentumOptimizer(learning_rate, 0.9, use_nesterov=True)
                        else:
                            raise ValueError('Invalid optimization algorithm')

                        grads = opt.compute_gradients(total_loss, update_gradient_vars)

                        tower_grads.append(grads)

                        reuse = True
                        if i == 0:
                            update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS,
                                                           scope)

        embed_group = tf.concat(embeddings_set, 0)
        grads = facenet.average_gradients(tower_grads)

        apply_gradient_op = [opt.apply_gradients(grads, global_step=global_step)]
        apply_gradient_op.extend(update_ops)

        # Attach a scalar summmary to all individual losses and the total loss; do the
        # same for the averaged version of the losses.
        for l in [total_loss]:
            # Name each loss as '(raw)' and name the moving average version of the loss
            # as the original loss name.
            tf.summary.scalar(l.op.name.split('/')[-1] + ' (raw)', l)
            # tf.summary.scalar(l.op.name.split('/')[-1] + '_1', loss_averages.average(l))

        # Add histograms for trainable variables.
        if args.log_histograms:
            for var in tf.trainable_variables():
                tf.summary.histogram(var.op.name, var)

        # Add histograms for gradients.
        if args.log_histograms:
            for grad, var in grads:
                if grad is not None:
                    tf.summary.histogram(var.op.name + '/gradients', grad)

        # Track the moving averages of all trainable variables.
        variable_averages = tf.train.ExponentialMovingAverage(
            args.moving_average_decay, global_step)
        variables_averages_op = variable_averages.apply(tf.trainable_variables())
        apply_gradient_op.append(variables_averages_op)
        train_op = tf.group(*apply_gradient_op)

        # Create a saver
        saver = tf.train.Saver(tf.trainable_variables(), max_to_keep=3)

        # Build the summary operation based on the TF collection of Summaries.
        summary_op = tf.summary.merge_all()

        # Start running operations on the Graph.
        gpu_options = tf.GPUOptions(allow_growth=True)
        sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options, log_device_placement=False,
                                                allow_soft_placement=True))
        # Initialize variables
        sess.run(tf.global_variables_initializer(), feed_dict={phase_train_placeholder: True})
        sess.run(tf.local_variables_initializer(), feed_dict={phase_train_placeholder: True})

        summary_writer = tf.summary.FileWriter(log_dir, sess.graph)

        with sess.as_default():

            if args.pretrained_model:
                print('Restoring pretrained model: %s' % args.pretrained_model)
                saver.restore(sess, os.path.expanduser(args.pretrained_model))

            # Training and validation loop
            epoch = 0
            while epoch < args.max_nrof_epochs:

                step = sess.run(global_step, feed_dict=None)
                epoch = step // args.epoch_size
                if epoch % args.data_load_cycle == 0:
                    rand_idx = np.random.permutation(len(train_set))
                    train_set = [train_set[i_index] for i_index in rand_idx[:args.people_per_batch]]
                    train_imgs = load_dataset(args, train_set)

                train(args, sess, train_imgs, epoch, image_placeholder, learning_rate_placeholder,
                      phase_train_placeholder, global_step, embed_group, total_loss, train_op, summary_op,
                      summary_writer
                      , args.embedding_size)
                evaluation(args, sess, embed_group, test_imgs, image_placeholder,
                           phase_train_placeholder, global_step, summary_writer)
                # Save variables and the metagraph if it doesn't exist already
                save_variables_and_metagraph(sess, saver, summary_writer, model_dir, subdir, step)

    return model_dir


def train(args, sess, dataset, epoch, image_placeholder,
          learning_rate_placeholder, phase_train_placeholder, global_step,
          embeddings, loss, train_op, summary_op, summary_writer, embedding_size):
    batch_number = 0

    lr = args.learning_rate
    selection_time = 0
    step = 0
    while batch_number < args.epoch_size:
        # Perform training on the selected triplets
        start_time = time.time()
        triplets, _ = select_triplets(args, sess, embeddings, dataset,
                                      image_placeholder, phase_train_placeholder, learning_rate_placeholder)
        selection_time = time.time() - start_time

        start_time = time.time()
        feed_dict = {image_placeholder: triplets, learning_rate_placeholder: lr,
                     phase_train_placeholder: True}
        if batch_number % 100 == 0:
            err, _, step, summary_str = sess.run([loss, train_op, global_step, summary_op], feed_dict=feed_dict)
            summary_writer.add_summary(summary_str, global_step=step)
        else:
            err, _, step = sess.run([loss, train_op, global_step], feed_dict=feed_dict)
        duration = time.time() - start_time
        print('Epoch: [%d][%d/%d]\tselect_triplets %.3f \tTime %.3f\tLoss %2.3f' %
              (epoch, batch_number + 1, args.epoch_size, selection_time, duration,
               err))
        batch_number += 1

    # Add validation loss and accuracy to summary
    summary = tf.Summary()
    # pylint: disable=maybe-no-member
    summary.value.add(tag='time/selection', simple_value=selection_time)
    summary_writer.add_summary(summary, step)
    return step


def select_triplets(args, sess, embeddings, dataset, image_placeholder, phase_train_placeholder,
                    learning_rate_placeholder):
    start_time = time.time()
    rand_idx = randperm(len(dataset))
    triplets_size = args.batch_size / 3
    # negtive_size = 110
    dataset = [dataset[i] for i in rand_idx[:min(len(dataset), int(triplets_size))]]
    negative_size = min(args.negative_size, len(dataset) - 1)

    # print('init time: %.3f' % (time.time() - start_time))

    start_time = time.time()
    total_batch_tuple = []
    for i_batch in range(len(dataset)):
        img_path_len = len(dataset[i_batch])
        total_batch_tuple.append((i_batch, np.random.randint(img_path_len)))
        for i in range(int(args.images_per_person)):
            total_batch_tuple.append((i_batch, np.random.randint(img_path_len)))
        offset = 0

        for i in range(int(negative_size)):
            if i == i_batch:
                offset = 1
            img_path_len = len(dataset[i + offset])
            total_batch_tuple.append((i + offset, np.random.randint(img_path_len)))

    total_batch_size = len(list(set(total_batch_tuple)))
    odd_batch = False
    if total_batch_size % args.num_gpus != 0:
        odd_batch = True
        total_batch_size += args.num_gpus - (total_batch_size % args.num_gpus)
    # print(total_batch_size)
    total_batch = np.zeros((total_batch_size, args.image_size, args.image_size, 3))
    emb_arr = np.zeros((total_batch_size, args.embedding_size))
    index_map = {}

    start_idx = 0
    for i_batch in range(len(total_batch_tuple)):
        # index_name = '%d-%d' % (total_batch_tuple[i_batch][0], total_batch_tuple[i_batch][1])
        index = total_batch_tuple[i_batch]
        if total_batch_tuple[i_batch] not in index_map.keys():
            total_batch[start_idx, :, :, :] = facenet.prewhiten(dataset[index[0]][index[1]])
            index_map[total_batch_tuple[i_batch]] = start_idx
            start_idx += 1

    # print('data prepare time: %.3f' % (time.time() - start_time))

    start_time = time.time()
    batch_size = int(args.batch_size)
    nof_batches = int(np.ceil(float(total_batch_size) / batch_size))
    for idx in range(nof_batches):
        right_bound = min((idx + 1) * batch_size, total_batch_size)
        feed_dict = {image_placeholder: total_batch[idx * batch_size: right_bound, :, :, :],
                     phase_train_placeholder: True, learning_rate_placeholder: args.learning_rate}
        emb_arr[idx * batch_size: right_bound, :] = sess.run(embeddings, feed_dict=feed_dict)

    # print('data process time: %.3f' % (time.time() - start_time))

    start_time = time.time()
    triplets = np.zeros((int(len(dataset) * 3), args.image_size, args.image_size, 3))
    # anchors = np.zeros((int(triplets_size), args.image_size, args.image_size, 3))
    # positives = np.zeros((int(triplets_size), args.image_size, args.image_size, 3))
    # negatives = np.zeros((int(triplets_size), args.image_size, args.image_size, 3))

    start_idx = 0
    i_triplet = 0
    for idx in range(int(len(dataset))):
        anchor = emb_arr[index_map[total_batch_tuple[start_idx]], :]
        triplets[i_triplet, :, :, :] = total_batch[index_map[total_batch_tuple[start_idx]], :, :, :]

        embed_p = []
        for i_embed in range(start_idx + 1, start_idx + 1 + int(args.images_per_person)):
            embed_p.append(emb_arr[index_map[total_batch_tuple[i_embed]]])
        embed_p = np.asarray(embed_p)

        embed_n = []
        for i_embed in range(start_idx + 1 + int(args.images_per_person), start_idx + int(
                                args.images_per_person + negative_size + 1)):
            embed_n.append(emb_arr[index_map[total_batch_tuple[i_embed]]])
        embed_n = np.asarray(embed_n)

        ap_dist = np.sum(np.square(anchor - embed_p), axis=1)
        p_index = np.argmax(ap_dist)
        triplets[i_triplet + 1, :, :, :] = total_batch[index_map[total_batch_tuple[start_idx + 1 + p_index]], :, :, :]

        an_dist = np.sum(np.square(anchor - embed_n), axis=1)
        n_index = np.argmin(an_dist)
        triplets[i_triplet + 2, :, :, :] = total_batch[
                                           index_map[total_batch_tuple[
                                               start_idx + 1 + int(args.images_per_person) + n_index]], :, :, :]

        start_idx += int(args.images_per_person + negative_size + 1)
        i_triplet += 3

    # triplets[0::3, :, :, :] = anchors
    # triplets[1::3, :, :, :] = positives
    # triplets[2::3, :, :, :] = negatives
    # print('mining time: %.3f' % (time.time() - start_time))

    return triplets, len(triplets)


def evaluation(args, sess, embeddings, dataset, image_placeholder,
               phase_train_placeholder, global_step, summary_writer):
    start_time = time.time()
    test_size = min(len(dataset), args.test_size)
    print('Test:')
    image_batch_size = test_size * 20
    if image_batch_size % args.num_gpus != 0:
        image_batch_size += args.num_gpus - (image_batch_size % args.num_gpus)
    image_batch = np.zeros((image_batch_size, args.image_size, args.image_size, 3))
    label_batch = []
    rand_idx = np.random.permutation(len(dataset))
    start_idx = 0
    for i in range(test_size):
        imgs = dataset[rand_idx[i]]
        rand_pos_idx = np.random.permutation(len(imgs))

        for it in range(min(5, int(len(rand_pos_idx) / 2))):
            image_batch[start_idx, :, :, :] = facenet.prewhiten(imgs[rand_pos_idx[it]])
            image_batch[start_idx + 1, :, :, :] = \
                facenet.prewhiten(imgs[rand_pos_idx[it + min(5, int(len(rand_pos_idx) / 2))]])
            label_batch.append(True)
            start_idx += 2
        for it in range(min(5, len(imgs))):
            neg_idx = np.random.randint(0, len(dataset), size=1)[0]
            while neg_idx == rand_idx[i]:
                neg_idx = np.random.randint(0, len(dataset), size=1)[0]
            image_batch[start_idx, :, :, :] = facenet.prewhiten(imgs[it])
            image_batch[start_idx + 1, :, :, :] = \
                facenet.prewhiten(dataset[neg_idx][np.random.randint(0, len(dataset[neg_idx]))])
            label_batch.append(False)
            start_idx += 2

    print('\tData prepare: %f' % (time.time() - start_time))

    nrof_batch = np.ceil(len(image_batch) / args.batch_size)
    emb_array = np.zeros((len(image_batch), args.embedding_size))

    batch_size = int(args.batch_size)
    for i in range(int(nrof_batch)):
        start_time = time.time()
        right_bound = min((i + 1) * batch_size, len(image_batch))

        feed_dict = {image_placeholder: image_batch[i * batch_size:right_bound], phase_train_placeholder: False}

        emb = sess.run(embeddings, feed_dict=feed_dict)
        emb_array[i * batch_size:right_bound, :] = emb
        print('\t(%d/%d): %f' % (i + 1, nrof_batch, (time.time() - start_time)))

    # _, _, accuracy, val, val_std, far = lfw.evaluate(emb_array, label_batch)

    thresholds = np.arange(0, 4, 0.01)
    embeddings1 = emb_array[0:test_size * 20:2]
    embeddings2 = emb_array[1:test_size * 20:2]
    assert (embeddings1.shape[0] == embeddings2.shape[0])
    assert (embeddings1.shape[1] == embeddings2.shape[1])

    diff = np.subtract(embeddings1, embeddings2)
    dist = np.sum(np.square(diff), 1)

    accuracy = []
    tps = []
    tns = []
    for idx in range(len(thresholds)):
        predict_issame = np.less(dist[:len(label_batch)], thresholds[idx])
        tp = float(np.sum(np.logical_and(predict_issame, label_batch)))
        fp = float(np.sum(np.logical_and(predict_issame, np.logical_not(label_batch))))
        tn = float(np.sum(np.logical_and(np.logical_not(predict_issame), np.logical_not(label_batch))))
        fn = float(np.sum(np.logical_and(np.logical_not(predict_issame), label_batch)))

        acc = (tp / np.sum(label_batch) + tn / np.sum(np.logical_not(label_batch))) / 2
        tps.append(tp / np.sum(label_batch))
        tns.append(tn / np.sum(np.logical_not(label_batch)))
        accuracy.append(acc)
    best_threshold_index = np.argmax(accuracy)
    print('Accuracy: %1.3f  tp: %1.3f  tn:%1.3f  threshold: %1.3f' % (accuracy[best_threshold_index],
                                                                      tps[best_threshold_index],
                                                                      tns[best_threshold_index],
                                                                      thresholds[best_threshold_index]))

    step = sess.run(global_step)
    summary = tf.Summary()
    # pylint: disable=maybe-no-member
    summary.value.add(tag='accuracy/accuracy', simple_value=accuracy[best_threshold_index])
    summary.value.add(tag='accuracy/tp', simple_value=tps[best_threshold_index])
    summary.value.add(tag='accuracy/tn', simple_value=tns[best_threshold_index])
    summary.value.add(tag='threshold', simple_value=thresholds[best_threshold_index])
    summary_writer.add_summary(summary, step)


def save_variables_and_metagraph(sess, saver, summary_writer, model_dir, model_name, step):
    # Save the model checkpoint
    print('Saving variables')
    start_time = time.time()
    checkpoint_path = os.path.join(model_dir, 'model-%s.ckpt' % model_name)
    saver.save(sess, checkpoint_path, global_step=step, write_meta_graph=False)
    save_time_variables = time.time() - start_time
    print('Variables saved in %.2f seconds' % save_time_variables)
    metagraph_filename = os.path.join(model_dir, 'model-%s.meta' % model_name)
    save_time_metagraph = 0
    if not os.path.exists(metagraph_filename):
        print('Saving metagraph')
        start_time = time.time()
        saver.export_meta_graph(metagraph_filename)
        save_time_metagraph = time.time() - start_time
        print('Metagraph saved in %.2f seconds' % save_time_metagraph)
    summary = tf.Summary()
    # pylint: disable=maybe-no-member
    summary.value.add(tag='time/save_variables', simple_value=save_time_variables)
    summary.value.add(tag='time/save_metagraph', simple_value=save_time_metagraph)
    summary_writer.add_summary(summary, step)


def load_dataset(args, dataset):
    loaded_imgs = []
    if args.multiprocess:
        nof_people = len(dataset)

        image_list, label_list = facenet.get_image_paths_and_labels(dataset)

        data_set = zip(image_list, range(len(image_list)),
                       [len(image_list)] * (len(image_list)))

        proc = facenet.Processor()
        pool = multiprocessing.Pool()
        train_set = np.asarray(pool.map(proc, data_set))

        for idx in range(nof_people):
            loaded_imgs.append(train_set[np.asarray(label_list) == idx])
    else:
        for i_img in range(len(dataset)):
            image_set = np.zeros((len(dataset[i_img].image_paths), args.image_size, args.image_size, 3),
                                 dtype=np.uint8)
            for i_idx in range(len(dataset[i_img].image_paths)):
                image_set[i_idx, :, :, :] = misc.imread(dataset[i_img].image_paths[i_idx])
            loaded_imgs.append(image_set)
            print('Loading images: %.1f%%' % (float(i_img + 1) / len(dataset) * 100))

    return loaded_imgs


def parse_arguments(argv):
    parser = argparse.ArgumentParser()

    parser.add_argument('--alpha', type=float,
                        help='Positive to negative triplet distance margin.', default=0.2)
    parser.add_argument('--logs_base_dir', type=str,
                        help='Directory where to write event logs.', default='../logs/')
    parser.add_argument('--models_base_dir', type=str,
                        help='Directory where to write trained models and checkpoints.', default='../models/')
    parser.add_argument('--pretrained_model', type=str,
                        help='Load a pretrained model before training starts.')
    parser.add_argument('--data_dir', type=str,
                        help='Path to the data directory containing aligned face patches. Multiple directories are '
                             'separated with colon.')
    parser.add_argument('--model_def', type=str,
                        help='Model definition. Points to a module containing the definition of the inference graph.',
                        default='models.inception_resnet_v1')
    parser.add_argument('--max_nrof_epochs', type=int,
                        help='Number of epochs to run.', default=50000)
    parser.add_argument('--batch_size', type=int,
                        help='Number of images to process in a batch.', default=90)
    parser.add_argument('--image_size', type=int,
                        help='Image size (height, width) in pixels.', default=160)
    parser.add_argument('--people_per_batch', type=int,
                        help='Number of people per batch.', default=1000)
    parser.add_argument('--test_people', type=int,
                        help='Number of people per batch.', default=5000)
    parser.add_argument('--images_per_person', type=int,
                        help='Number of images per person.', default=12)
    parser.add_argument('--negative_size', type=int,
                        help='Number of negative per person.', default=110)
    parser.add_argument('--face_pool_size', type=int,
                        help='Number of face.', default=500)
    parser.add_argument('--epoch_size', type=int,
                        help='Number of batches per epoch.', default=1000)
    parser.add_argument('--embedding_size', type=int,
                        help='Dimensionality of the embedding.', default=128)
    parser.add_argument('--keep_probability', type=float,
                        help='Keep probability of dropout for the fully connected layer(s).', default=1.0)
    parser.add_argument('--weight_decay', type=float,
                        help='L2 weight regularization.', default=0.0)
    parser.add_argument('--optimizer', type=str, choices=['SGD', 'ADAGRAD', 'ADADELTA', 'ADAM', 'RMSPROP', 'MOM'],
                        help='The optimization algorithm to use', default='SGD')
    parser.add_argument('--learning_rate', type=float,
                        help='Initial learning rate. If set to a negative value a learning rate ' +
                             'schedule can be specified in the file "learning_rate_schedule.txt"', default=0.1)
    parser.add_argument('--learning_rate_decay_epochs', type=int,
                        help='Number of epochs between learning rate decay.', default=100)
    parser.add_argument('--learning_rate_decay_factor', type=float,
                        help='Learning rate decay factor.', default=1.0)
    parser.add_argument('--log_histograms',
                        help='Enables logging of weight/bias histograms in tensorboard.', action='store_true')
    parser.add_argument('--seed', type=int,
                        help='Random seed.', default=777)
    parser.add_argument('--center_loss_factor', type=float,
                        help='Center loss factor.', default=0.0)
    parser.add_argument('--center_loss_alfa', type=float,
                        help='Center update rate for center loss.', default=0.95)
    parser.add_argument('--moving_average_decay', type=float,
                        help='Exponential decay for tracking of training parameters.', default=0.9999)
    parser.add_argument('--num_gpus', type=int,
                        help='Number of gpu to be used.')
    parser.add_argument('--test_size', type=int,
                        help='Number of test samples per test.', default=1000)
    parser.add_argument('--data_load_cycle', type=int,
                        help='Number of test samples per test.', default=100)
    parser.add_argument('--variable_strategy', choices=['CPU', 'GPU'], type=str,
                        default='CPU', help='Where to locate variable operations')
    parser.add_argument('--multiprocess',
                        help='Enables multiprocess when loading data.', action='store_true')

    return parser.parse_args(argv)


if __name__ == '__main__':
    main(parse_arguments(sys.argv[1:]))

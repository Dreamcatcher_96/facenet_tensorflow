import tensorflow as tf
import numpy as np
import facenet
import os
import time
from scipy import misc
import models.inception_resnet_v1 as network
import tensorflow.contrib.slim as slim

data_dir = '/media/alan/Data/Face_data/test/images_BB'
model_path = '/home/alan/workspace/Facenet_tensorflow/models/20171115-181356/model-20171115-181356.ckpt-%s000'

ckpts = [246, 247]


def evaluate(sess, image_list, label_list, image_placeholder, phase_train_placeholder,
             logits):
    batch_size = 80
    test_size = len(image_list) // batch_size
    test_batch = [batch_size] * test_size
    if batch_size * test_size != len(image_list):
        test_batch += [len(image_list) - batch_size * test_size]
        test_size += 1
    image_size = 150
    print('Test: ')

    # data_prepare_time = time.time()
    # randperm = np.random.permutation(len(image_list))
    #
    # image_list = [image_list[i] for i in randperm]
    # label_list = [label_list[i] for i in randperm]
    #
    # data_prepare_time = time.time() - data_prepare_time

    # print('Data_prepare: %f' % data_prepare_time)

    acc = 0
    for i in range(test_size):
        data_process_time = time.time()
        image_batch = np.zeros((test_batch[i], image_size, image_size, 3))
        label_batch = np.zeros((test_batch[i]))

        for i_img in range(test_batch[i]):
            img = misc.imread(image_list[i * batch_size + i_img])
            image_batch[i_img] = facenet.prewhiten(img)
            label_batch[i_img] = label_list[i * batch_size + i_img]

        feed_dict = {phase_train_placeholder: False,
                     image_placeholder: image_batch}  # , label_placeholder: label_batch}

        data_process_time = time.time() - data_process_time

        inference_time = time.time()
        predict = sess.run(logits, feed_dict=feed_dict)

        predict = np.argmax(predict, axis=1)

        inference_time = time.time() - inference_time

        acc += sum(
            np.equal(predict, label_batch).astype('int')) / float(len(predict))

        print('\t(%d/%d) Data_process: %f\tInference: %f' % (i + 1, test_size, data_process_time, inference_time))

    print('Accuracy: %f' % (acc / test_size))

    return acc / test_size


accs = []
for i_ckpt in ckpts:
    with tf.Graph().as_default():
        with tf.Session() as sess:
            all_data_set = facenet.get_dataset(data_dir)
            test_set = []
            for item in all_data_set:
                paths = item.image_paths
                class_name = item.name

                index = np.arange(len(paths))

                test_path = [paths[i] for i in index[int(np.ceil(len(paths) * 0.7)):]]

                test_set.append(facenet.ImageClass(class_name, test_path))

            test_image_list, test_label_list = facenet.get_image_paths_and_labels(test_set)

            # facenet.load_model(model_path)

            images_placeholder = tf.placeholder(tf.float32, shape=(None, 150, 150, 3),
                                                name='image_data')
            # logits = tf.get_default_graph().get_tensor_by_name('Logits/BiasAdd:0')
            phase_train_placeholder = tf.placeholder(tf.bool, name='phase_train')

            prelogits, _ = network.inference(images_placeholder, 1,
                                             phase_train=phase_train_placeholder)
            logits = slim.fully_connected(prelogits, 2000, activation_fn=None,
                                          weights_initializer=tf.truncated_normal_initializer(stddev=0.1),
                                          weights_regularizer=slim.l2_regularizer(0.0),
                                          scope='Logits', reuse=False)

            # sess.run(tf.initialize_all_variables())

            saver = tf.train.Saver(tf.trainable_variables())

            saver.restore(sess, model_path % i_ckpt)

            # variables = tf.trainable_variables()
            #
            # server_weight = np.load('weight_when_save.npy')
            #
            # # for item in variables:
            # weight = sess.run(variables[0]._variable)
            # np.save('weight', weight)

            acc = evaluate(sess, test_image_list, test_label_list, images_placeholder, phase_train_placeholder, logits)

            accs.append(acc)

print accs

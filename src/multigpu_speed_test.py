import time

import numpy as np
import six
import tensorflow as tf
from tensorflow.core.framework import node_def_pb2
from tensorflow.python.framework import device as pydev
from tensorflow.python.training import device_setter

import facenet
import models.inception_resnet_v1 as network

seed = 777
image_size = 150
keep_probability = 0.9
embedding_size = 128
weight_decay = 5e-5
alpha = 0.2
epoch_size = 1000
variable_strategy = 'CPU'


def local_device_setter(num_devices=1,
                        ps_device_type='cpu',
                        worker_device='/cpu:0',
                        ps_ops=None,
                        ps_strategy=None):
    if ps_ops is None:
        ps_ops = ['Variable', 'VariableV2', 'VarHandleOp']

    if ps_strategy is None:
        ps_strategy = device_setter._RoundRobinStrategy(num_devices)
    if not six.callable(ps_strategy):
        raise TypeError("ps_strategy must be callable")

    def _local_device_chooser(op):
        current_device = pydev.DeviceSpec.from_string(op.device or "")

        node_def = op if isinstance(op, node_def_pb2.NodeDef) else op.node_def
        if node_def.op in ps_ops:
            ps_device_spec = pydev.DeviceSpec.from_string(
                '/{}:{}'.format(ps_device_type, ps_strategy(op)))

            ps_device_spec.merge_from(current_device)
            return ps_device_spec.to_string()
        else:
            worker_device_spec = pydev.DeviceSpec.from_string(worker_device or "")
            worker_device_spec.merge_from(current_device)
            return worker_device_spec.to_string()

    return _local_device_chooser


num_gpus = 2
times = []
samples = []
with tf.Graph().as_default(), tf.device('/cpu:0'):
    tf.set_random_seed(seed)
    global_step = tf.Variable(0, trainable=False)

    # Placeholder for the learning rate
    learning_rate_placeholder = tf.placeholder(tf.float32, name='learning_rate')

    phase_train_placeholder = tf.placeholder(tf.bool, name='phase_train')

    image_placeholder = tf.placeholder(tf.float32, shape=(None, image_size, image_size, 3),
                                       name='image_data')

    # label_placeholder = tf.placeholder(tf.int64, shape=(None,), name='label_data')

    image_split = tf.split(image_placeholder, num_gpus)
    # label_split = tf.split(label_placeholder, num_gpus)

    learning_rate = tf.train.exponential_decay(learning_rate_placeholder, global_step,
                                               100 * epoch_size,
                                               1.0, staircase=True)
    tf.summary.scalar('learning_rate', learning_rate)

    tower_grads = []
    reuse = False
    embeddings_set = []
    if num_gpus == 0:
        num_devices = 1
        device_type = 'cpu'
    else:
        num_devices = num_gpus
        device_type = 'gpu'
    for i in xrange(num_gpus):
        worker_device = '/{}:{}'.format(device_type, i)
        if variable_strategy == 'CPU':
            model_device_setter = local_device_setter(
                worker_device=worker_device)
        elif variable_strategy == 'GPU':
            model_device_setter = local_device_setter(
                ps_device_type='gpu',
                worker_device=worker_device,
                ps_strategy=tf.contrib.training.GreedyLoadBalancingStrategy(
                    num_gpus, tf.contrib.training.byte_size_load_fn))
        with tf.variable_scope(tf.get_variable_scope()):
            with tf.name_scope('tower_%d' % i) as scope:
                with tf.device(model_device_setter):
                    # Build the inference graph
                    prelogits, _ = network.inference(image_split[i], keep_probability,
                                                     phase_train=phase_train_placeholder,
                                                     bottleneck_layer_size=embedding_size,
                                                     weight_decay=weight_decay, reuse=reuse)

                    # prelogits = conv_net(image_split[i], embedding_size, keep_probability,
                    #                      reuse=reuse, is_training=phase_train_placeholder)

                    # prelogits, _ = network.inception_resnet_v1(image_split[i], is_training=phase_train_placeholder,
                    #                                            reuse=reuse)

                    embeddings = tf.nn.l2_normalize(prelogits, 1, 1e-10, name='embeddings')

                    embeddings_set.append(embeddings)

                    # Split embeddings into anchor, positive and negative and calculate triplet loss
                    anchor, positive, negative = tf.unstack(tf.reshape(embeddings, [-1, 3, embedding_size]), 3,
                                                            1)
                    triplet_loss = facenet.triplet_loss(anchor, positive, negative, alpha)

                    # Calculate the total losses
                    regularization_losses = tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES)
                    total_loss = tf.add_n([triplet_loss] + regularization_losses, name='total_loss')

                    # Build a Graph that trains the model with one batch of examples and updates the model
                    # parameters
                    # loss_averages = tf.train.ExponentialMovingAverage(0.9, name='avg')
                    # losses = tf.get_collection('losses', scope)
                    # loss_averages_op = loss_averages.apply(losses + [total_loss])

                    optimizer = 'SGD'
                    update_gradient_vars = tf.global_variables()
                    opt = tf.train.GradientDescentOptimizer(learning_rate)
                    grads = opt.compute_gradients(total_loss, update_gradient_vars)

                    tower_grads.append(grads)

                    reuse = True
                    if i == 0:
                        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS,
                                                       scope)

    embed_group = tf.concat(embeddings_set, 0)
    grads = facenet.average_gradients(tower_grads)

    apply_gradient_op = [opt.apply_gradients(grads, global_step=global_step)]
    apply_gradient_op.extend(update_ops)

    variable_averages = tf.train.ExponentialMovingAverage(
        0.9999, global_step)
    variables_averages_op = variable_averages.apply(tf.trainable_variables())
    apply_gradient_op.append(variables_averages_op)
    train_op = tf.group(*apply_gradient_op)

    gpu_options = tf.GPUOptions(allow_growth=True)
    sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options, log_device_placement=False,
                                            allow_soft_placement=True))
    # Initialize variables
    sess.run(tf.global_variables_initializer(), feed_dict={phase_train_placeholder: True})
    sess.run(tf.local_variables_initializer(), feed_dict={phase_train_placeholder: True})

    image_batch = np.random.rand(10000)
    nrof_batch = np.ceil(len(image_batch) / args.batch_size)

    total_time = 0
    total_sample = 0
    for i in range(100):
        input_array = np.random.rand(30 * 3 * num_gpus, 150, 150, 3)
        start_time = time.time()
        sess.run(embeddings_set,
                 feed_dict={image_placeholder: input_array, phase_train_placeholder: False,
                            learning_rate_placeholder: 0.1})
        duration = time.time() - start_time
        total_time += duration
        total_sample += 30 * 3 * num_gpus / duration
        print '%d/%d  time: %.3f, %.3f Examples/sec' % (i + 1, 100, duration, 30 * num_gpus / duration)
    print 'avg time: %.3f  avg Examples/sec: %.3f' % (total_time / 100, total_sample / 100)
    times.append(total_time / 100)
    samples.append(total_sample / 100)

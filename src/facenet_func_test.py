from datetime import datetime
import os
import facenet
import tensorflow as tf
import models.inception_resnet_v1 as network
import tensorflow.contrib.slim as slim

data_path = '/home/alan/workspace/Facenet_tensorflow/models/20171115-181356/model-20171115-181356.ckpt-245000'
model_dir = '/home/alan/workspace/Facenet_tensorflow/models'

with tf.Graph().as_default():
    with tf.Session() as sess:
        # facenet.load_model(data_path)

        images_placeholder = tf.placeholder(tf.float32, shape=(None, 150, 150, 3),
                                            name='image_data')
        # logits = tf.get_default_graph().get_tensor_by_name('Logits/BiasAdd:0')
        phase_train_placeholder = tf.placeholder(tf.bool, name='phase_train')

        prelogits, _ = network.inference(images_placeholder, 1,
                                         phase_train=phase_train_placeholder)
        logits = slim.fully_connected(prelogits, 2000, activation_fn=None,
                                      weights_initializer=tf.truncated_normal_initializer(stddev=0.1),
                                      weights_regularizer=slim.l2_regularizer(0.0),
                                      scope='Logits', reuse=False)

        model_loader = tf.train.Saver(tf.trainable_variables())

        model_loader.restore(sess, data_path)

        variables = tf.trainable_variables()[:-2]
        subdir = datetime.strftime(datetime.now(), '%Y%m%d-%H%M%S')
        saver = tf.train.Saver(variables)
        checkpoint_path = os.path.join(model_dir, 'model-%s.ckpt' % subdir)
        saver.save(sess, checkpoint_path, global_step=0, write_meta_graph=False)

import os
from numpy import random
import facenet
import numpy as np

data_2000 = '/media/alan/Data/Face_data/images_BB'
data_36174 = '/media/alan/Data/Face_data/face_data_washed_aligned'

class_name = os.listdir(data_2000)

all_set = facenet.get_dataset(data_36174)

used_idx = []
for i in range(len(all_set)):
    if all_set[i].name in class_name:
        used_idx.append(i)

index = range(len(all_set))
index = list(set(index) - set(used_idx))

rand_idx = np.random.permutation(len(index))
index = [index[i] for i in rand_idx]

ridx = np.random.permutation(len(used_idx))
used_idx = [used_idx[i] for i in ridx]

index += used_idx

np.save('../data/train_test_idx.npy', index)

# all_name = os.listdir(data_36174)
# class_name = os.listdir(data_2000)
#
# names_without_class = set(all_name) - set(class_name)
# names_without_class = list(names_without_class)
# names_without_class.sort()
# class_name.sort()
#
# all_set = []
# for item in names_without_class:
#     classname = item
#     image_paths = os.listdir(os.path.join(data_36174, item))
#     all_set.append(facenet.ImageClass(classname, image_paths))
#
# extra_set = []
# for item in class_name:
#     classname = item
#     image_paths = os.listdir(os.path.join(data_36174, item))
#     extra_set.append(facenet.ImageClass(classname, image_paths))
#
# rand_idx = np.random.permutation(len(all_set))
# all_data_set = [all_set[i] for i in rand_idx]
#
# rand_idx = np.random.permutation(len(extra_set))
# extra_set = [extra_set for i in rand_idx]
#
# all_data_set += extra_set



# list_data = '/home/alan/workspace/facenet/data/face_qualified.txt'
#
# dirs = os.listdir(path)
#
# # overlap_list = []
# with open(list_data, 'rb') as li:
#     content = li.read().split('\r\n')
#     # for item in content:
#     #     if item in dirs:
#     #         overlap_list.append(item)
#     overlap_list = set.intersection(set(content), set(dirs))
#
# print len(overlap_list)
# selected = random.choice(overlap_list, 2000)
#
# print len(selected)
#
# if not os.path.exists(os.path.join(list_data, '../', 'face_data_classify')):
#     os.mkdir(os.path.join(list_data, '../', 'face_data_classify'))
# for item in selected:
#     shutil.copytree(os.path.join(path, item), os.path.join(list_data, '../', 'face_data_classify', item))

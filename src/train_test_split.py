import shutil

import facenet
import os
import numpy as np
import random

data_dir = '/media/alan/Data/Face_data/images_BB'

all_data_set = facenet.get_dataset(data_dir)

train_set = []
test_set = []
for item in all_data_set:
    paths = item.image_paths
    class_name = item.name

    index = np.arange(len(paths))

    train_path = [paths[i] for i in index[:int(np.ceil(len(paths) * 0.7))]]
    test_path = [paths[i] for i in index[int(np.ceil(len(paths) * 0.7)):]]

    train_set.append(facenet.ImageClass(class_name, train_path))
    test_set.append(facenet.ImageClass(class_name, test_path))

image_dir = train_set[0].image_paths[0].split('/')[:-2]
image_dir.append('train.txt')
image_dir[0] = ' '
with open(os.path.join(*image_dir)[1:], 'wb') as txt:
    for item in train_set:
        # os.remove(os.path.join(*image_dir)[1:])
        for i_path in item.image_paths:
            txt.write('%s\r\n' % i_path)

image_dir = test_set[0].image_paths[0].split('/')[:-2]
image_dir.append('test.txt')
image_dir[0] = ' '
with open(os.path.join(*image_dir)[1:], 'wb') as txt:
    for item in test_set:
        # os.remove(os.path.join(*image_dir)[1:])
        for i_path in item.image_paths:
            txt.write('%s\r\n' % i_path)
